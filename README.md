# AMSOM AutocompleteCity

Ce package propose un composant de saisie de la ville/pays/cp.

## Installation

```bash
npm i @amsom-habitat/amsom-autocomplete-city
```

## Développment

Après avoir fait vos dev, veillez à bien tenir à jour le [changelog.md](changelog.md) ainsi que la version du package.json puis faites :
```bash
git add .
git commit -m '<commentaire'
git push origin <branch>
```

## Tests

Les tests sont réalisé de manière automatique sur les branches main et dev mais peuvent être fait localement, notemment pour voir l'evolution du développement via la commande :
```bash
npm run storybook
```

Le valideur devra, si des changements sont observés, aller sur la pipeline pour valider les différences à l'aide de chromatic, sans cela aucun merge-request ne sera possible. Si un merge est effectué, une double verification sera necessaire.

## Déploiement

Après avoir merge les dev sur la branche main, exécutez :
```bash
make publish
```
Cette commande vérifie la version, le changelog et publie le tout

## Utilisation

#### Props

- `communeColClass` : Col boostrap de la commune
- `cpColClass` : Col boostrap du code postal
- `paysColClass` : Col boostrap du pays
- `forceShowZipCode` : Force l'affichage du code postal ou non
- `askEnFrance` : Demande si la ville est en France ou non
- `required` : Indique si la commune est requise
- `zipCodeRequired` : Indique si le code postal est requis
- `abroardZipCodeRequired` : Indique si le code postal à l'étranger est requis
- `modelValue` : Objet contenant les valeurs de la ville, du code postal et du pays
- `noResultString` : Texte à afficher si la ville n'est pas en France
- `placeholder` : Placeholder de la ville
- `maxlength` : Longueur maximale de la ville
- `readOnly` : Indique si le champ est en lecture seule
- `defaultDepartement` : Département par défaut pour la recherche
- `paysOptions` : Options du pays
- `inputIdentifier` : Identifiant du champ
- `cityLabel` : Label du champ de la ville


#### Example complet
```tsx
<tempate>
    <amsom-autocomplete-city 
      v-model="city"
      required
      zip-code-required
      no-result-string="Cliquez ici pour indiquer une commune située en dehors de la France"
      :pays-options="paysOptions"
    />
</template>

<script>
import {AmsomAutocompleteCity} from '@amsom-habitat/autocomplete-city'
import { sortArray } from "@/js/array.js";
import { mapState } from "vuex";
import { normalize } from "@/js/text.js";

  export default {
    name: 'TestPage',
    components: {
      AmsomAutocompleteCity,
    },
    data() {
        return {
            city: null
        }
    },
    computed: {
    ...mapState(['utilities']),
    paysOptions(){
        if (!this.utilities.pays) return [];
        
        let data = this.utilities.pays
        .filter((pays) => pays.id !== "FRA")
        .map((pays) => {
          return { id: pays.id, nom: normalize(pays.nom, false) };
        });
        
        return sortArray(data, "nom", true);
    }
  }
}
</script>
```
